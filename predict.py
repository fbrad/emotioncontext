import os, argparse, json
import torch, codecs
from allennlp.data.vocabulary import Vocabulary
from allennlp.common import Params
from allennlp.data.dataset_readers import DatasetReader
from allennlp.models import Model
from allennlp.commands.train import datasets_from_params
from allennlp.commands.predict import Predictor
from emo_dataset_reader import EmoDatasetReader, CustomWordSplitter
from emotion_predictor import EmotionPredictor
from emo_model import EmoModel

parser = argparse.ArgumentParser()
parser.add_argument('-experiment_dir', default='saved_models/', type=str,
						help='path to experiment dir where models are stored')
parser.add_argument('-test_file', default='data/test_twitter_cleanup.csv', type=str,
						help='path to test file')
parser.add_argument('-batch_size', default=64, type=int, help='batch size')

if __name__ == '__main__':
	args = parser.parse_args()

	#load Param object from config.json file found in experiment directory
	config_path = os.path.join(args.experiment_dir, "config.json")
	with codecs.open(config_path, "r", encoding='UTF-8') as f:
		params_dict = json.load(f)
	params = Params(params = params_dict)
	dataset_reader_params = params.duplicate() #hack so that "dataset_reader" dict is kept

	#load Dataset Reader
	reader = DatasetReader.from_params(params=dataset_reader_params.pop("dataset_reader"))
	reader.predict()

	#training_params
	training_params = params.pop("trainer")

	#load datasets
	all_datasets = datasets_from_params(params)
	datasets_for_vocab_creation = set(params.pop("datasets_for_vocab_creation", all_datasets))

	vocab = Vocabulary.from_params(
		params.pop("vocabulary", {}),
		(instance for key, dataset in all_datasets.items()
		 for instance in dataset
		 if key in datasets_for_vocab_creation)
	)

	#load model
	model = Model.from_params(vocab=vocab, params=params.pop('model'))

	#load weights from specific epoch
	num_epochs = training_params.pop("num_epochs")
	for epoch_number in range(num_epochs):
		print("Predictions from model at epoch ", epoch_number)
		submission_path = os.path.join(args.experiment_dir, "predicted_epoch_%d.txt" % epoch_number)
		model_state_path = os.path.join(args.experiment_dir, 'model_state_epoch_%d.th' % epoch_number)
		model_state = torch.load(model_state_path)
		model.load_state_dict(model_state)

		predictor = EmotionPredictor(model, reader, args.batch_size)
		predictor.prepare_submission(args.test_file, submission_path)