import tempfile, os, argparse
from torch import nn, optim
from emo_dataset_reader import EmoDatasetReader, CustomWordSplitter
from emotion_predictor import EmotionPredictor
from allennlp.nn.util import get_text_field_mask
from allennlp.common.file_utils import cached_path
from allennlp.data.vocabulary import Vocabulary
from allennlp.data.dataset import Batch
from allennlp.data.dataset_readers import DatasetReader
from allennlp.data.iterators import BucketIterator
from allennlp.modules.token_embedders.elmo_token_embedder import ElmoTokenEmbedder
from allennlp.modules.token_embedders.token_characters_encoder import TokenCharactersEncoder
from allennlp.modules.text_field_embedders.basic_text_field_embedder import BasicTextFieldEmbedder
from allennlp.modules.seq2seq_encoders.pytorch_seq2seq_wrapper import PytorchSeq2SeqWrapper
from allennlp.models import Model
from allennlp.predictors import Predictor
from allennlp.data.tokenizers.word_splitter import SpacyWordSplitter
from allennlp.data.tokenizers.word_splitter import WordSplitter
from allennlp.common.params import Params
from allennlp.commands.train import train_model

from emo_model import EmoModel
from allennlp.training.trainer import Trainer

parser = argparse.ArgumentParser()
parser.add_argument('-train_file', default='data/train.txt', type=str,
						help='path to train file')
parser.add_argument('-val_file', default='data/dev.txt', type=str,
						help='path to evaluation file')
parser.add_argument('-test_file', default='data/test_twitter_cleanup.csv', type=str,
						help='path to evaluation file')
parser.add_argument('-config_file', default='config_files/exp_1.jsonnet', type=str,
						help='path to config file')
parser.add_argument('-experiment_basedir', default='saved_models', type=str,
						help='path to base folder where experiments folders are saved')
parser.add_argument('-elmo_options_file', type=str, help="path to ELMo JSON options file",
		default="https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/"
		"2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json")
parser.add_argument('-elmo_weight_file', type=str, help="path to ELMo hdf5 weights file",
		default="https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/"
		"2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5")

parser.add_argument('-encoder_hidden_size', default=1024, type=int)
parser.add_argument('-batch_size', default=32, type=int)
parser.add_argument('-num_epochs', default=2, type=int)
parser.add_argument('-cuda_id', default=0, type=int, help="-1 for CPU, >=0 for GPU number")

def build_trainer_from_args(args):
	"""
	Build Trainer object (and all dependent objects) from command line arguments
	:param args:
	:return: Trainer
	"""
	#read datasets into Instances
	reader = EmoDatasetReader(word_splitter=CustomWordSplitter())
	train_instances = reader.read(cached_path(args.train_file))
	val_instances = reader.read(cached_path(args.val_file))

	#create vocabulary and link iterator to it
	vocab = Vocabulary.from_instances(train_instances + val_instances)#, min_count={"elmo_characters": 1})
	iterator = BucketIterator(batch_size = args.batch_size, sorting_keys=[("turns", "num_tokens")])
	iterator.index_with(vocab)

	#initialize Modules and assemble them in Model

	#initialize embedder Modules
	elmo = ElmoTokenEmbedder(options_file=args.elmo_options_file,
							 weight_file=args.elmo_weight_file, dropout=0.5)
	elmo_embedder = BasicTextFieldEmbedder({"elmo_character_ids" : elmo})

	#initialize encoder Module
	lstm = PytorchSeq2SeqWrapper(nn.LSTM(input_size=elmo.get_output_dim(),
										 hidden_size=args.encoder_hidden_size,
										 batch_first=True))
	#initialize Model
	model = EmoModel(elmo_embedder, lstm, vocab)

	optimizer = optim.SGD(model.parameters(), lr=0.1)

	trainer = Trainer(model=model,
                  optimizer=optimizer,
                  iterator=iterator,
                  train_dataset=train_instances,
                  validation_dataset=val_instances,
                  patience=10,
                  num_epochs=args.num_epochs,
				  serialization_dir=args.experiment_dir,
				  cuda_device=args.cuda_id)

	return trainer, reader

def get_experiment_name(config_full_path:str) -> str:
	"""
	Returns the experiment name from the
	:param config_full_path: e.g. "config_files/experiment_2.jsonnet"
	:return: experiment name "experiment_2"
	"""
	config_basename = os.path.basename(config_full_path)  # config/exp.jsonnet -> exp_1.jsonnet
	experiment_name = os.path.splitext(config_basename)[0]  # exp_1.jsonnet -> exp_1

	return experiment_name


if __name__ == '__main__':
	#parse arguments
	args = parser.parse_args()
	args.experiment_dir = os.path.join(args.experiment_basedir, get_experiment_name(args.config_file))

	#Trainer.train() does not serialize vocabulary => Model.load() will not work out of the box
	#trainer, reader = build_trainer_from_args(args)
	#metrics = trainer.train()
	#print(metrics)

	#load Model based on configuration and saved weights
	experiment_config = Params.from_file(args.config_file)

	#alternatively call train_model() with jsonnet config file and load best model as a result
	#use the allennlp command tool
	best_model = train_model(experiment_config, args.experiment_dir)#, recover=True)

	#setup Predictor
	#best_model = Model.load(config=experiment_config, serialization_dir=args.experiment_dir,
	#						cuda_device=args.cuda_id)
	reader = EmoDatasetReader(word_splitter=CustomWordSplitter())
	predictor = EmotionPredictor(best_model, reader, args.batch_size)
	predictor.prepare_submission(args.test_file, os.path.join(args.experiment_dir, "best.txt"))