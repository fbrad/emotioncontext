import os
import codecs
import numpy as np

def compute_class_weights(train_file:str,
						  label_vocab_file: str = 'saved_models/exp_1/vocabulary/labels.txt'):
	"""
	Compute an array of length #num_classes, where each value is used to weight the
	log-likelihood.
	:param train_file: path to training file
		   vocab_file: path to label vocabulary file (saved in $exp_name/vocabulary/label.txt)
	:return:
	"""
	label_idx = {} #stores label->index mapping
	with open(label_vocab_file, "r") as f:
		for idx, line in enumerate(f):
			label = line.strip()
			label_idx[label] = idx

	labels = []  # stores label for each example in training set
	with open(train_file, "r") as f:
		for line in f.readlines():
			toks = line.strip().split()
			label = toks[-1]
			assert label in ["others", "angry", "sad", "happy"], "invalid label"
			labels.append(label_idx[label])

	#compute label frequency
	label_freq = np.bincount(labels).astype(np.float64)
	print("[compute_class_weights] label_freq = ", label_freq, " label_idx = ", label_idx)
	labels_weights = len(labels) / (len(label_idx) * label_freq)

	return labels_weights


def undersample_majority_class(train_file:str):
	"""
	:param train_file: path to training file
	:return: saves another file "balanced_$train_file", where the majority class has been
	undersampled
	"""
	#build a hashmap with labels as keys and arrays of examples as keys
	label_line_dict = {"others": [], "angry": [], "sad": [], "happy": []}
	with codecs.open(train_file, "r", encoding='UTF-8') as f:
		for line in f.readlines():
			toks = line.strip().split()
			label = toks[-1]
			assert label in ["others", "angry", "sad", "happy"], "invalid label"

			label_line_dict[label].append(line)

	label_freq = {k: len(v) for k,v in label_line_dict.items()}
	labels = list(label_freq.keys())
	label_freq_list = list(label_freq.values())

	max_frequency = max(label_freq_list)
	label_most_common = labels[label_freq_list.index(max_frequency)]
	print("Most common label is ", label_most_common, " and has frequency ", max_frequency)

	#pop most frequent label and compute the average frequency of the other labels.
	#most frequent examples will be capped at this average frequency.
	label_freq.pop(label_most_common)
	print("New label_freq: ", label_freq)
	other_labels_freq = list(label_freq.values())
	avg_label_freq = sum(other_labels_freq) // len(other_labels_freq)
	print("Average frequency of minority labels is ",avg_label_freq)

	output_basedir = os.path.dirname(train_file)
	output_basename = "balanced_" + os.path.basename(train_file)
	output_file = os.path.join(output_basedir, output_basename)

	with codecs.open(output_file, "w", encoding='UTF-8') as f:
		for label, lines in label_line_dict.items():
			if label == label_most_common:
				f.writelines(lines[:avg_label_freq])
			else:
				f.writelines(lines)

if __name__ == '__main__':
	#undersample_majority_class("data/train.txt")
	print(compute_class_weights("data/train.txt"))
