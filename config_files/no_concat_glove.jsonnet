local label_weights = [0.5051448, 1.37172323, 1.38353146, 1.75879364];
local uniform_weights = [1.0, 1.0, 1.0, 1.01];
{
	"train_data_path": "data/train_twitter_cleanup.csv",
	"validation_data_path": "data/dev_twitter_cleanup.csv",
	"dataset_reader": {
		"type": "emo-reader",
		"token_indexers": {
			"elmo_character_ids": {
				"type": "elmo_characters"
			},
			"tokens": {
				"type": "single_id",
				"lowercase_tokens": true
			}
		},
		"word_splitter": {
			"type": "custom-splitter"
		}
	},
	"model" : {
		"type": "emo-model",
		"embedder": {
			"type": "basic",
			"token_embedders": {
				"elmo_character_ids" : {
					"type": "elmo_token_embedder",
					"options_file": "https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_options.json",
					"weight_file": "https://s3-us-west-2.amazonaws.com/allennlp/models/elmo/2x4096_512_2048cnn_2xhighway/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5",
					"dropout": 0.5
				},
				"tokens": {
					"type": "embedding",
					"pretrained_file": "data/glove.twitter.27B.200d.txt",
					"embedding_dim": 200,
					"trainable": true
				}
			}
		},
		"encoder": {
			"type": "lstm",
			"input_size": 1224,
			"hidden_size": 1024,
			"bidirectional": true
		},
		"classifier_feedforward": {
		    "input_dim": 2048,
		    "num_layers": 3,
            "hidden_dims": [400, 100, 4],
            "activations": ["relu", "relu", "linear"],
            "dropout": [0.0, 0.5, 0.0]
		},
		"label_weights": uniform_weights,
		"regularizer": [
            [
                ".*scalar_parameters.*",
                {
                    "type": "l2",
                    "alpha": 0.001
                }
            ]
        ],
		"concat_type": "none"
	},
	"iterator": {
		"type": "bucket",
		"batch_size": 32,
		"sorting_keys": [["turns", "num_tokens"]]
	},
	"trainer": {
	    "validation_metric": "+macro_f1",
		"optimizer": {
			"type": "dense_sparse_adam",
			"lr": 0.001
		},
		"learning_rate_scheduler": {
			"type": "reduce_on_plateau",
			"patience": 1
		},
		"grad_clipping": 5.0,
		"num_epochs": 10,
		"cuda_device": 0
	}
}
