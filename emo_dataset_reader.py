from typing import Dict, List, Iterator

import re, codecs
from allennlp.data import Instance
from allennlp.data.fields import TextField, LabelField, IndexField
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer
from allennlp.data.token_indexers.elmo_indexer import ELMoTokenCharactersIndexer
from allennlp.data.token_indexers.token_characters_indexer import TokenCharactersIndexer
from allennlp.data.tokenizers.word_splitter import WordSplitter, SpacyWordSplitter
from allennlp.data.tokenizers import Token
from allennlp.data.dataset_readers import DatasetReader

@DatasetReader.register('emo-reader')
class EmoDatasetReader(DatasetReader):
	"""
	Dataset Reader for EmoContext examples at SemEval 2019:
		train line: 15	Money money and lots of money😍😍	😍	😁😁	happy
		val line: 7	Where I ll check	why tomorrow?	No I want now
	"""
	def __init__(self, token_indexers: Dict[str, TokenIndexer] = None,
				 word_splitter: WordSplitter = SpacyWordSplitter()) -> None:
		super().__init__(lazy=False)
		self.training = True
		self.word_splitter = word_splitter
		self.token_indexers = token_indexers or \
							  {"elmo_character_ids": ELMoTokenCharactersIndexer(),
							   "tokens": SingleIdTokenIndexer()
							   }

	def predict(self):
		"""
		Sets the reader in predict mode (no gold labels available)
		"""
		return self.train(False)

	def train(self, mode=True):
		"""
		Sets the reader in training mode (gold labels available)
		"""
		self.training = mode
		return self

	def text_to_instance(self, text: List[Token], label: str = None) -> Instance:
		#this field contains the actual sentences (turns) tokens
		turn_field = TextField(text, self.token_indexers)

		#these fields contains the indices (absolute positions in the full sequence) of the ending
		#Tokens in each of the three turns; used to retrieve the final hidden state of each
		#turn in the dialogue
		turn_end_indices = [idx for idx, tok in enumerate(text) if tok.text == '<end>']
		assert len(turn_end_indices) == 3, "Missing turn terminators: %d" % len(turn_end_indices)
		turn1_end_field = IndexField(turn_end_indices[0], turn_field)
		turn2_end_field = IndexField(turn_end_indices[1], turn_field)

		fields = {"turns": turn_field,
				  "turn1_end_idx": turn1_end_field,
				  "turn2_end_idx": turn2_end_field}

		if label:
			label_field = LabelField(label)
			fields["labels"] = label_field

		return Instance(fields)

	def _read(self, file_path: str) -> Iterator[Instance]:
		with codecs.open(file_path, "r", encoding='UTF-8') as f:
			for line in f.readlines():
				all_tokens = self.word_splitter.split_words(line)

				#discard first two tokens: $line_number and <end>
				if self.training:
					tokens, label = all_tokens[2:-1], all_tokens[-1].text

				else:
					tokens, label = all_tokens, None

				yield self.text_to_instance(tokens, label)


@WordSplitter.register("custom-splitter")
class CustomWordSplitter(WordSplitter):
	"""
		Aggresive word splitter
	"""
	def __init__(self):
		self.pattern = '([\[\](){}*@&=#,.:;%$\\\'`^"_\-–²+|<>?!/\s])'
		self.filter_toks = {'\xa0', '', ' ', '\n'}

	def split_words(self, sentence: str) -> List[Token]:
		sentence_tokens = list(filter(lambda tok: tok not in self.filter_toks,
							    re.split(self.pattern, sentence.strip())))

		#replace TABS with separator <end>
		sentence_tokens = [tok if tok != '\t' else '<end>' for tok in sentence_tokens]

		return [Token(tok) for tok in sentence_tokens]

