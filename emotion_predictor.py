from allennlp.common.util import JsonDict
from allennlp.data import Instance
from allennlp.predictors import Predictor
from allennlp.models.model import Model
from allennlp.data.dataset_readers.dataset_reader import DatasetReader
from allennlp.data.iterators.basic_iterator import BasicIterator
import codecs

@Predictor.register("emo-predictor")
class EmotionPredictor(Predictor):
	def __init__(self, model: Model, dataset_reader: DatasetReader, batch_size: int) -> None:
		super().__init__(model, dataset_reader)
		self.word_splitter = dataset_reader.word_splitter
		self.index_to_class = {0: "others", 1: "angry", 2: "sad", 3: "happy"}
		self.batch_size = batch_size

	def _json_to_instance(self, json_dict: JsonDict) -> Instance:
		"""
		Expects JSON that looks like ``{"sentence": "..."}``.
		Runs the underlying model, and adds the ``"words"`` to the output.
		"""
		sentence = json_dict["sentence"]
		self._dataset_reader.predict()
		tokens = self.word_splitter.split_words(sentence)
		return self._dataset_reader.text_to_instance(tokens)

	def predict(self, sentence: str) -> str:
		"""
		Given a sentence (line from dev.txt or test.txt), return the predicted class
		as a string ("others", "angry", "sad" or "happy")

		"""
		#{'label_logits': [1.7338056564331055, -0.4565294086933136, 2.812303066253662,
		#				  -4.161435127258301]}
		label_logits = self.predict_json({"sentence": sentence})["label_logits"]
		max_label_idx = label_logits.index(max(label_logits))

		return self.index_to_class[max_label_idx]

	def prepare_submission(self, test_path: str, predicted_path: str) -> None:
		"""
		Given a test file (devwithoutlabels.txt or test.txt), append predictions to each line
		and write them to another file.
		Arguments: test_path: path to file with examples that need predictions
				   predicted_path: path to file containing the test examples and the appended
									predictions
		"""
		with codecs.open(test_path, "r", encoding='UTF-8') as f:
			test_lines = f.readlines()

		self._dataset_reader.predict()
		test_instances = self._dataset_reader._read(test_path)

		#create BasicIterator
		it = BasicIterator(batch_size=self.batch_size)
		batches = it._create_batches(test_instances, shuffle=False)

		#decode batches and save predictions
		predicted_labels = []
		for idx, batch in enumerate(batches):
			print(idx * self.batch_size, " predictions completed")
			batch_instances = list(iter(batch))
			batch_predictions = self.predict_batch_instance(batch_instances)

			batch_logits = [pred["label_logits"] for pred in batch_predictions]
			batch_max_label_idx = [pred_logits.index(max(pred_logits)) for pred_logits in batch_logits]
			batch_max_label = [self.index_to_class[label_idx] for label_idx in batch_max_label_idx]
			predicted_labels.extend(batch_max_label)

		#write predictions to file
		with codecs.open(predicted_path, "w", encoding='UTF-8') as f:
			f.write("id\tturn1\tturn2\tturn3\tlabel\n")

			for line, predicted_label in zip(test_lines, predicted_labels):
				f.write(line.strip() + '\t' + predicted_label + '\n')

		return None



