from typing import Dict

import torch
from torch import nn
from allennlp.nn.util import get_text_field_mask, get_final_encoder_states
from allennlp.data.vocabulary import Vocabulary
from allennlp.models import Model
from allennlp.modules.elmo import Elmo
from allennlp.modules.seq2seq_encoders.pytorch_seq2seq_wrapper import Seq2SeqEncoder
from allennlp.modules.text_field_embedders.basic_text_field_embedder import TextFieldEmbedder
from allennlp.modules.feedforward import FeedForward
from allennlp.training.metrics import F1Measure, Average
from allennlp.nn.regularizers import RegularizerApplicator
from torch.nn import ReLU

@Model.register('emo-model')
class EmoModel(Model):
	"""
		label_weights: array of class weights
		interaction_type: value in ['none', 'elementwise', 'difference']
			'elementwise': hidden states of turns are multiplied elementwise
	"""
	def __init__(self, embedder: TextFieldEmbedder,
				 encoder: Seq2SeqEncoder,
				 classifier_feedforward: FeedForward,
				 vocab: Vocabulary,
				 label_weights: list = [],
				 regularizer: RegularizerApplicator = None,
				 concat_type: str = 'none') -> None:
		super().__init__(vocab, regularizer)
		self.embedder = embedder
		self.encoder = encoder
		assert concat_type in ['none', 'pool', 'all'], "Unknown concat type"
		self.concat_type = concat_type
		num_representations = 1 if concat_type == 'none' else 3
		classifier_input_dim = classifier_feedforward.get_input_dim()
		encoder_output_dim = num_representations * encoder.get_output_dim()
		assert  encoder_output_dim == classifier_input_dim, \
			"Dimension mismatch, encoder = %d, classifier = %d" % (encoder_output_dim,
																classifier_input_dim)
		self.hidden2label = classifier_feedforward
		self.label_weights = torch.tensor(label_weights)
		self.criterion = nn.CrossEntropyLoss(weight=self.label_weights)
		self.vocab = vocab
		self.f1_angry = F1Measure(positive_label=1) #TO DO: add support for microF1
		self.f1_sad = F1Measure(positive_label=2)
		self.f1_happy = F1Measure(positive_label=3)

	def forward(self, turns: Dict[str, torch.Tensor], turn1_end_idx: torch.Tensor,
				turn2_end_idx: torch.Tensor, labels: torch.Tensor = None) -> torch.Tensor:
		"""
		Parameters:
			turns: {"character_ids": Tensor(B x T x 50)}
			turn1_end_idx: Tensor(B, 1)
			turn2_end_idx: Tensor(B, 1)
			labels: Tensor(B)
		"""
		#B x T
		mask = get_text_field_mask(turns)

		#B x T x emb_dim
		emb = self.embedder(turns)

		#B x T x hidden_dim
		memory = self.encoder(emb, mask)

		#retrieve hidden state at the end of turn1
		current_batch_size = memory.size(0)
		batch_range = torch.arange(current_batch_size)
		turn1_end_idx = turn1_end_idx.squeeze()
		turn2_end_idx = turn2_end_idx.squeeze()

		#B x hidden_dim
		turn1_end_hidden = memory[batch_range, turn1_end_idx, :]
		turn2_end_hidden = memory[batch_range, turn2_end_idx, :]
		turn3_end_hidden = get_final_encoder_states(memory, mask)

		#B x (num_representations * hidden_dim)
		if self.concat_type == 'none':
			hidden_interact = turn3_end_hidden
		elif self.concat_type == 'all':
			hidden_interact = torch.cat((turn1_end_hidden, turn2_end_hidden, turn3_end_hidden),
											 dim=1)
		else:
			max_pooled_memory = torch.max(memory, dim=1)[0]
			mean_pooled_memory = torch.mean(memory, dim=1)
			hidden_interact = torch.cat((max_pooled_memory, mean_pooled_memory, turn3_end_hidden),
										dim=1)

		#B x num_classes
		label_logits = self.hidden2label(hidden_interact)

		#feedforward classifier
		output = {"label_logits": label_logits}

		if labels is not None:
			self.f1_angry(label_logits, labels)
			self.f1_sad(label_logits, labels)
			self.f1_happy(label_logits, labels)
			output["loss"] = self.criterion(label_logits, labels)

		return output

	def get_metrics(self, reset: bool = False) -> Dict[str, float]:
		f1_angry = self.f1_angry.get_metric(reset)[-1]
		f1_sad = self.f1_sad.get_metric(reset)[-1]
		f1_happy = self.f1_happy.get_metric(reset)[-1]

		return {"f1_angry": f1_angry,
				"f1_sad": f1_sad,
				"f1_happy": f1_happy,
				"macro_f1": (f1_angry + f1_sad + f1_happy) / 3
				}